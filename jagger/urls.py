# django
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import login, logout

# our code
from jagger.views import (
    AccountView,
    AccountStatsView,
    AccountSpreadsheetView,
    AccountUploadCsvView,
    EntryCreate,
    EntryDelete,
    EntryUpdate,
    index,
)

urlpatterns = [
    url(r"^admin/", admin.site.urls),
    url(
        r"^login/$",
        login,
        {"template_name": "admin/login.html"},
        name="login",
    ),
    url(r"^logout/$", logout, name="logout", kwargs={"next_page": "/"}),
    url(r"^$", index, name="index"),
    url(r"^accounts/(?P<pk>\d+)$", AccountView.as_view(), name="entry_list"),
    url(r"^accounts/(?P<pk>\d+)/stats$", AccountStatsView.as_view(), name="stats"),
    url(
        r"^accounts/(?P<pk>\d+)/spreadsheet$",
        AccountSpreadsheetView.as_view(),
        name="download_spreadsheet",
    ),
    url(
        r"^accounts/(?P<pk>\d+)/upload_csv$",
        AccountUploadCsvView.as_view(),
        name="upload_csv",
    ),
    url(
        r"^accounts/(?P<account_id>\d+)/entries/create$",
        EntryCreate.as_view(),
        name="entry_create",
    ),
    url(
        r"^accounts/(?P<account_id>\d+)/entries/(?P<pk>\d+)$",
        EntryUpdate.as_view(),
        name="entry_update",
    ),
    url(
        r"^accounts/(?P<account_id>\d+)/entries/(?P<pk>\d+)/delete$",
        EntryDelete.as_view(),
        name="entry_delete",
    ),
]
