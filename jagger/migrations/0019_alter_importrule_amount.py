# Generated by Django 3.2.5 on 2021-07-07 16:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("jagger", "0018_importrule_amount"),
    ]

    operations = [
        migrations.AlterField(
            model_name="importrule",
            name="amount",
            field=models.DecimalField(
                blank=True, decimal_places=2, max_digits=7, null=True
            ),
        ),
    ]
