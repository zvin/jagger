# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-22 16:55
from __future__ import unicode_literals

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("jagger", "0010_auto_20161222_1651"),
    ]

    operations = [
        migrations.AlterField(
            model_name="account",
            name="owner",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL
            ),
        ),
        migrations.AlterField(
            model_name="tax",
            name="account",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="jagger.Account"
            ),
        ),
    ]
