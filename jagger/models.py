# stdlib
from decimal import ROUND_HALF_UP, Decimal

# django
from django.contrib.auth.models import User
from django.db import models
from django.db.models import F, Func, Max, Sum
from django_countries.fields import CountryField
from djchoices import ChoiceItem, DjangoChoices

# 3rdparty
from tabulate import tabulate


def round_to_cent(d):
    return d.quantize(Decimal("0.01"), ROUND_HALF_UP)


class Tax(models.Model):
    class Meta:
        verbose_name_plural = "taxes"

    name = models.CharField(max_length=64)
    rate = models.DecimalField(max_digits=3, decimal_places=3)
    country = CountryField()
    account = models.ForeignKey("Account", on_delete=models.CASCADE)

    def __str__(self):
        return "{} ({}): {}%".format(
            self.name,
            self.country.name,
            round_to_cent(self.rate * 100),
        )


class Round(Func):
    function = "ROUND"
    template = "%(function)s(%(expressions)s, 2)"


class Account(models.Model):
    name = models.CharField(max_length=512)
    initial_balance = models.DecimalField(max_digits=9, decimal_places=2)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def entries(self, start=None, end=None):
        entries = self.entry_set.all()
        if start is not None:
            entries = entries.filter(date__gte=start)
        if end is not None:
            entries = entries.filter(date__lte=end)
        return entries

    def total(self, start=None, end=None, *args, **kwargs):
        entries = self.entries(start, end).filter(*args, **kwargs)
        return entries.aggregate(Sum("amount"))["amount__sum"] or Decimal()

    def _tax_amount(self, country, ledger_type, start=None, end=None):
        entries = self.entries(start, end).filter(
            tax__country=country,
            ledger__type=ledger_type,
        )
        return (
            entries.aggregate(
                tax_total=Sum(
                    Round(
                        F("professional_part")
                        * (F("amount") - F("amount") / (1 + F("tax__rate")))
                    ),
                )
            )["tax_total"]
            or Decimal()
        )

    def bank_balance(self, end=None):
        bank_in = self.entries(None, end).filter(ledger__type=Ledger.LedgerType.IN)
        bank_out = self.entries(None, end).filter(ledger__type=Ledger.LedgerType.OUT)
        return (
            +self.initial_balance
            + bank_in.aggregate(Sum("amount"))["amount__sum"]
            - bank_out.aggregate(Sum("amount"))["amount__sum"]
        )

    def tax_collected(self, country, start=None, end=None):
        return self._tax_amount(country, Ledger.LedgerType.IN, start, end)

    def tax_spent(self, country, start=None, end=None):
        return self._tax_amount(country, Ledger.LedgerType.OUT, start, end)

    def tax_adjusted(self, country, start=None, end=None):
        entries = self.entries(start, end).filter(vat_adjustment_country=country)
        return entries.aggregate(Sum("amount"))["amount__sum"] or Decimal()

    def tax_due(self, country, start=None, end=None):
        return (
            +self.tax_collected(country, start, end)
            - self.tax_spent(country, start, end)
            - self.tax_adjusted(country, start, end)
        )

    def tax_due_fr(self, start=None, end=None):
        return self.tax_due("FR", start, end)

    def last_entry_date(self):
        return self.entries().aggregate(Max("date"))["date__max"]

    def stats(self, start=None, end=None):
        return tabulate(
            (
                (
                    ledger.type,
                    ledger.name,
                    ledger.number,
                    self.total(start, end, ledger=ledger),
                )
                for ledger in Ledger.objects.all()
                if self.total(start, end, ledger=ledger) != 0
            ),
            floatfmt=".2f",
        )


class Ledger(models.Model):
    class Meta:
        ordering = ["type", "number"]

    class LedgerType(DjangoChoices):
        IN = ChoiceItem("IN")
        OUT = ChoiceItem("OUT")

    name = models.CharField(max_length=512)
    number = models.IntegerField()
    type = models.CharField(
        max_length=3,
        choices=LedgerType.choices,
        validators=[LedgerType.validator],
        blank=True,
    )

    def __str__(self):
        return "{} - {}".format(self.number, self.name)


class Entry(models.Model):
    class Meta:
        verbose_name_plural = "entries"
        ordering = ["date", "id"]

    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    date = models.DateField()
    description = models.CharField(max_length=512)
    notes = models.TextField(blank=True)
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    professional_part = models.DecimalField(
        max_digits=3,
        decimal_places=2,
        default=Decimal(1),
    )
    ledger = models.ForeignKey(Ledger, on_delete=models.CASCADE)
    tax = models.ForeignKey(Tax, null=True, blank=True, on_delete=models.SET_NULL)
    vat_adjustment_country = CountryField(blank=True)

    @property
    def tax_amount(self):
        if self.tax is not None:
            return round_to_cent(
                self.professional_part
                * (self.amount - self.amount / (1 + self.tax.rate))
            )


class Attachment(models.Model):
    entry = models.ForeignKey(Entry, on_delete=models.CASCADE)
    document = models.FileField()


class ImportRule(models.Model):
    class MatchType(DjangoChoices):
        EXACT = ChoiceItem("EXACT")
        REGEX = ChoiceItem("REGEX")

    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    ledger = models.ForeignKey(Ledger, on_delete=models.CASCADE)
    professional_part = models.DecimalField(
        max_digits=3,
        decimal_places=2,
        default=Decimal(1),
    )
    tax = models.ForeignKey(Tax, null=True, blank=True, on_delete=models.SET_NULL)
    vat_adjustment_country = CountryField(blank=True)
    match_type = models.CharField(
        max_length=5,
        choices=MatchType.choices,
        validators=[MatchType.validator],
        default=MatchType.EXACT,
    )
    match = models.CharField(max_length=512)
    amount = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    description = models.CharField(max_length=512, blank=True)

    def __str__(self):
        return "{} {}".format(
            self.match_type,
            self.description,
        )
