# django
from django.contrib import admin
from reversion.admin import VersionAdmin

# our code
from jagger.models import Account, Attachment, Entry, ImportRule, Ledger, Tax


@admin.register(Account)
class AccountAdmin(VersionAdmin):
    list_display = ("name",)


@admin.register(Attachment)
class AttachmentAdmin(VersionAdmin):
    pass


@admin.register(Entry)
class EntryAdmin(VersionAdmin):
    list_display = (
        "date",
        "description",
        "amount",
        "ledger",
        "tax",
        "professional_part",
        "vat_adjustment_country",
    )
    search_fields = ("description", "notes")


@admin.register(Ledger)
class LedgerAdmin(VersionAdmin):
    list_display = ("number", "name", "type")


@admin.register(Tax)
class TaxAdmin(VersionAdmin):
    list_display = ("name", "country", "rate")


@admin.register(ImportRule)
class ImportRuleAdmin(VersionAdmin):
    list_display = (
        "account",
        "match_type",
        "match",
        "amount",
        "description",
        "ledger",
        "professional_part",
        "tax",
    )
