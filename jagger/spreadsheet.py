# stdlib
from datetime import timedelta
from itertools import chain, groupby
from tempfile import NamedTemporaryFile

# 3rdparty
import xlsxwriter
from xlsxwriter.utility import xl_col_to_name, xl_range, xl_rowcol_to_cell

# our code
from jagger.models import Ledger


def sum_column(sheet, column, first_row, last_row, sum_row, fmt):
    sheet.write_formula(
        xl_rowcol_to_cell(sum_row, column),
        "SUM({})".format(xl_range(first_row, column, last_row, column)),
        fmt,
    )


ACCUMULATION_ROW = 28
MONTH_TOTAL_ROW = 26
REIMBURSED_VAT_LEDGER = 624
PREVIOUS_MONTH_REPORT_ROW = 3
FIRST_ROW = 4
LAST_ROW = 25
COLUMN_WIDTH = 10


def spreadsheet(account):
    # Use CTRL + SHIFT + F9 in libreoffice to recalculate formulas.
    result = NamedTemporaryFile()
    in_ledgers = Ledger.objects.filter(type=Ledger.LedgerType.IN)
    out_ledgers = Ledger.objects.filter(type=Ledger.LedgerType.OUT)
    COLLECTED_VAT_COLUMN = in_ledgers.count()
    BANK_IN_COLUMN = COLLECTED_VAT_COLUMN + 1
    DATE_COLUMN = BANK_IN_COLUMN + 1
    LABEL_COLUMN = DATE_COLUMN + 1
    LEDGER_COLUMN = LABEL_COLUMN + 1
    BANK_OUT_COLUMN = LEDGER_COLUMN + 1
    PAID_VAT_COLUMN = BANK_OUT_COLUMN + 1
    ledger_columns = {}
    book = xlsxwriter.Workbook(result.name)
    centered_format = book.add_format({"align": "center"})
    balance_format = book.add_format(
        {
            "color": "green",
            "bold": True,
            "num_format": "0.00",
        }
    )
    balance_title_format = book.add_format(
        {
            "bold": True,
            "color": "green",
            "align": "center",
        }
    )
    accumulation_format = book.add_format(
        {
            "bold": True,
            "color": "brown",
            "num_format": "0.00",
        }
    )
    accumulation_title_format = book.add_format(
        {
            "align": "center",
            "color": "brown",
            "bold": True,
        }
    )
    bold_centered_format = book.add_format(
        {
            "bold": True,
            "align": "center",
        }
    )
    negative_format = book.add_format(
        {
            "num_format": "0.00",
            "color": "red",
        }
    )
    decimal_format = book.add_format({"num_format": "0.00"})
    groups = groupby(
        account.entries().order_by("date"),
        key=lambda e: e.date.replace(day=1),
    )
    for month_index, (month, entries) in enumerate(groups):
        previous_sheet = (month - timedelta(days=1)).strftime("%Y-%m")
        sheet = book.add_worksheet(month.strftime("%Y-%m"))
        sheet.set_column(
            "A:{}".format(xl_col_to_name(DATE_COLUMN)),
            COLUMN_WIDTH,
        )
        sheet.set_column(
            "{}:{}".format(
                xl_col_to_name(LEDGER_COLUMN),
                xl_col_to_name(LEDGER_COLUMN + out_ledgers.count() + 2),
            ),
            COLUMN_WIDTH,
        )
        sheet.merge_range(
            xl_range(0, 0, 0, COLLECTED_VAT_COLUMN),
            "ENTRÉES",
            centered_format,
        )
        sheet.merge_range(
            xl_range(
                0,
                PAID_VAT_COLUMN,
                0,
                PAID_VAT_COLUMN + out_ledgers.count(),
            ),
            "SORTIES",
            centered_format,
        )
        for column, ledger in enumerate(in_ledgers):
            ledger_columns[ledger.number] = column
            sheet.write(1, column, ledger.name)
            sheet.write(2, column, ledger.number, centered_format)
        sheet.write(1, COLLECTED_VAT_COLUMN, "TVA Collectée")
        sheet.write(1, BANK_IN_COLUMN, "Entrées")
        sheet.write(2, BANK_IN_COLUMN, "Banque", centered_format)
        # previous month bank account balance
        if month_index == 0:
            formula = account.initial_balance
        else:
            formula = "='{}'!{}".format(
                previous_sheet, xl_rowcol_to_cell(30, BANK_IN_COLUMN)
            )
        sheet.write(
            PREVIOUS_MONTH_REPORT_ROW,
            BANK_IN_COLUMN,
            formula,
            balance_format,
        )
        sheet.write(2, DATE_COLUMN, "Dates", centered_format)
        sheet.set_column("{0}:{0}".format(xl_col_to_name(LABEL_COLUMN)), 30)
        sheet.write(2, LABEL_COLUMN, "Libellés", centered_format)
        sheet.write(
            MONTH_TOTAL_ROW,
            LABEL_COLUMN,
            "Total pour le mois",
            bold_centered_format,
        )
        sheet.write(
            30,
            LABEL_COLUMN,
            "Solde fin de mois",
            bold_centered_format,
        )
        sheet.write(
            31,
            LABEL_COLUMN,
            "TVA due",
            bold_centered_format,
        )
        sheet.write(
            ACCUMULATION_ROW,
            LABEL_COLUMN,
            "Cumul",
            accumulation_title_format,
        )
        sheet.write(
            PREVIOUS_MONTH_REPORT_ROW,
            LABEL_COLUMN,
            "(+) Solde (-)",
            balance_title_format,
        )
        sheet.write(2, LEDGER_COLUMN, "N° Compte", centered_format)
        sheet.write(1, BANK_OUT_COLUMN, "Sorties")
        sheet.write(2, BANK_OUT_COLUMN, "Banque", centered_format)
        sheet.write(1, PAID_VAT_COLUMN, "TVA Déductible")
        for column, ledger in enumerate(out_ledgers, PAID_VAT_COLUMN + 1):
            ledger_columns[ledger.number] = column
            sheet.write(1, column, ledger.name)
            sheet.write(2, column, ledger.number, centered_format)
        # End of month bank balance
        sheet.write_formula(
            30,
            BANK_IN_COLUMN,
            "{} + {} - {}".format(
                xl_rowcol_to_cell(PREVIOUS_MONTH_REPORT_ROW, BANK_IN_COLUMN),
                xl_rowcol_to_cell(MONTH_TOTAL_ROW, BANK_IN_COLUMN),
                xl_rowcol_to_cell(MONTH_TOTAL_ROW, BANK_OUT_COLUMN),
            ),
            balance_format,
        )
        # French VAT due
        sheet.write_formula(
            31,
            BANK_IN_COLUMN,
            "{} - {} - {}".format(
                xl_rowcol_to_cell(ACCUMULATION_ROW, COLLECTED_VAT_COLUMN),
                xl_rowcol_to_cell(ACCUMULATION_ROW, PAID_VAT_COLUMN),
                xl_rowcol_to_cell(
                    ACCUMULATION_ROW,
                    ledger_columns[REIMBURSED_VAT_LEDGER],
                ),
            ),
            balance_format,
        )
        # Sums, accumulations #################################################
        columns_to_sum = [
            ledger_columns[l.number] for l in chain(in_ledgers, out_ledgers)
        ]
        # Collected VAT, Bank in, Bank out, Paid VAT
        columns_to_sum += [
            COLLECTED_VAT_COLUMN,
            BANK_IN_COLUMN,
            BANK_OUT_COLUMN,
            PAID_VAT_COLUMN,
        ]
        for column in columns_to_sum:
            # Monthly sums:
            sum_column(
                sheet,
                column,
                FIRST_ROW,
                LAST_ROW,
                MONTH_TOTAL_ROW,
                decimal_format,
            )
            # Accumulations:
            if month_index == 0:
                formula = xl_rowcol_to_cell(MONTH_TOTAL_ROW, column)
            else:
                formula = "'{}'!{} + {}".format(
                    previous_sheet,
                    xl_rowcol_to_cell(ACCUMULATION_ROW, column),
                    xl_rowcol_to_cell(MONTH_TOTAL_ROW, column),
                )
            sheet.write_formula(
                xl_rowcol_to_cell(ACCUMULATION_ROW, column),
                formula,
                accumulation_format,
            )
        #######################################################################
        for i, entry in enumerate(entries, FIRST_ROW):
            fmt = negative_format if entry.amount < 0 else decimal_format
            sheet.write(
                i,
                (
                    BANK_IN_COLUMN
                    if entry.ledger.type == Ledger.LedgerType.IN
                    else BANK_OUT_COLUMN
                ),
                entry.amount,
                fmt,
            )
            sheet.write(i, DATE_COLUMN, entry.date.day)
            sheet.write(i, LABEL_COLUMN, entry.description, fmt)
            sheet.write(i, LEDGER_COLUMN, entry.ledger.number)
            sheet.write(
                i,
                ledger_columns[entry.ledger.number],
                entry.amount,
                fmt,
            )
            if entry.tax is not None and entry.tax.country == "FR":
                sheet.write(
                    i,
                    (
                        COLLECTED_VAT_COLUMN
                        if entry.ledger.type == Ledger.LedgerType.IN
                        else PAID_VAT_COLUMN
                    ),
                    entry.tax_amount,
                    fmt,
                )
    book.close()
    result.seek(0)
    return result
