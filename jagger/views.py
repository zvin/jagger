import csv
import re
from datetime import datetime
from decimal import Decimal

# django
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_http_methods
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django_tables2 import Column, LinkColumn, RequestConfig, Table
from django_tables2.utils import A

# our code
from jagger.models import Account, Entry, ImportRule
from jagger.spreadsheet import spreadsheet


def entry_link_column(classes=None):
    attrs = {}
    if classes:
        attrs = {"td": {"class": classes}}
    return LinkColumn(
        "entry_update",
        kwargs={
            "account_id": A("account.id"),
            "pk": A("pk"),
        },
        attrs=attrs,
    )


class EntryTable(Table):
    class Meta:
        fields = [
            "date",
            "description",
            "amount",
            "ledger",
            "tax",
            "professional_part",
            "vat_adjustment_country",
        ]
        attrs = {"class": "table table-striped"}

    date = entry_link_column("col-xs-1")
    description = entry_link_column("col-xs-3")
    amount = entry_link_column("text-xs-right col-xs-1")
    ledger = entry_link_column("col-xs-3")
    tax = entry_link_column("col-xs-2")
    professional_part = entry_link_column("text-xs-right col-xs-1")
    vat_adjustment_country = entry_link_column("col-xs-1")


class OwnAccountView(LoginRequiredMixin, DetailView):
    model = Account

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(owner=self.request.user)


@method_decorator(require_http_methods(["GET"]), name="dispatch")
class AccountStatsView(OwnAccountView):
    template_name = "jagger/stats.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        start = self.request.GET.get("start")
        end = self.request.GET.get("end")
        context["stats"] = self.object.stats(
            start=start,
            end=end,
        )
        context["tax_collected"] = self.object.tax_collected(
            country="FR",
            start=start,
            end=end,
        )
        return context


@login_required
def index(request):
    accounts = Account.objects.filter(owner=request.user)
    if accounts.count() == 0:
        Account.objects.create(
            owner=request.user,
            name=request.user.username,
            initial_balance=0,
        )
        accounts = Account.objects.filter(owner=request.user)
    return redirect("entry_list", pk=request.accounts[0].pk)


@method_decorator(require_http_methods(["GET"]), name="dispatch")
class AccountView(OwnAccountView):
    template_name = "jagger/entry_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        account = context["account"]
        entries = account.entries()
        search = self.request.GET.get("search")
        if search:
            entries = entries.filter(
                Q(description__icontains=search) | Q(notes__icontains=search)
            )
        table = EntryTable(entries, order_by="-date")
        RequestConfig(self.request, paginate={"per_page": 10}).configure(table)
        context["table"] = table
        context["accounts"] = self.get_queryset()
        return context


class EntryMixin(LoginRequiredMixin):
    model = Entry
    fields = [
        "date",
        "amount",
        "description",
        "ledger",
        "tax",
        "vat_adjustment_country",
        "professional_part",
        "notes",
    ]

    def get_account(self):
        return Account.objects.filter(
            pk=self.request.resolver_match.kwargs["account_id"],
            owner=self.request.user,
        ).first()

    def get_context_data(self, **kwargs):
        context = super(EntryMixin, self).get_context_data(**kwargs)
        context["account"] = self.get_account()
        return context

    def get_success_url(self):
        account = self.object.account
        if "add-another" in self.request.POST:
            return reverse_lazy("entry_create", kwargs={"account_id": account.id})
        else:
            return reverse_lazy("entry_list", kwargs={"pk": account.id})

    def form_valid(self, form):
        messages.add_message(self.request, messages.INFO, self.success_message)
        return super(EntryMixin, self).form_valid(form)


class EntryCreate(EntryMixin, CreateView):
    success_message = "Entry created"

    def get_initial(self, *args, **kwargs):
        initial = super(EntryCreate, self).get_initial()
        last_date = self.get_account().last_entry_date()
        initial["date"] = last_date.strftime("%Y-%m-")
        return initial

    def form_valid(self, form):
        form.instance.account = self.get_account()
        return super(EntryCreate, self).form_valid(form)


class EntryUpdate(EntryMixin, UpdateView):
    success_message = "Entry updated"


class EntryDelete(EntryMixin, DeleteView):
    success_message = "Entry deleted"


@method_decorator(require_http_methods(["GET"]), name="dispatch")
class AccountSpreadsheetView(OwnAccountView):
    def render_to_response(self, context, **kwargs):
        return HttpResponse(
            spreadsheet(context["account"]).read(),
            content_type=(
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            ),
        )


def rule_matches(rule, row):
    return (rule.amount == None or rule.amount == row["amount"]) and (
        (rule.match_type == ImportRule.MatchType.EXACT and rule.match == row["label"])
        or (
            rule.match_type == ImportRule.MatchType.REGEX
            and re.match(rule.match, row["label"]) != None
        )
    )


def create_entry_from_rule(rule, row):
    return Entry(
        account=rule.account,
        ledger=rule.ledger,
        professional_part=rule.professional_part,
        tax=rule.tax,
        vat_adjustment_country=rule.vat_adjustment_country,
        description=rule.description or row["label"],
        amount=row["amount"],
        date=row["date"],
    )


class UnsavedEntryTable(Table):
    class Meta:
        model = Entry
        fields = [
            "date",
            "description",
            "amount",
            "ledger",
            "tax",
            "professional_part",
            "vat_adjustment_country",
        ]
        attrs = {"class": "table table-striped"}


class NoMatchTable(Table):
    class Meta:
        attrs = {"class": "table table-striped"}

    date = Column()
    label = Column()
    amount = Column()


def csv_import_ing(data, rules, insert=False):
    entries = []
    no_match = []
    for row in csv.reader(
        [line.decode("latin3") for line in data],
        delimiter=";",
    ):
        date = datetime.strptime(row[0], "%d/%m/%Y")
        label = row[1]
        amount = abs(Decimal(row[3].replace(",", ".")))
        row_data = {"date": date, "label": label, "amount": amount}
        for rule in rules:
            if rule_matches(rule, row_data):
                entry = create_entry_from_rule(rule, row_data)
                if insert:
                    entry.save()
                entries.append(entry)
                break
        else:
            no_match.append(row_data)
    return entries, no_match


def csv_import_wise(data, rules, insert=False):
    entries = []
    no_match = []
    first_row = True
    for row in csv.reader(
        [line.decode("latin3") for line in data],
        delimiter=",",
    ):
        if first_row:
            first_row = False
            continue
        date = datetime.strptime(row[1], "%d-%m-%Y")
        label = row[4]
        amount = abs(Decimal(row[2]))
        row_data = {"date": date, "label": label, "amount": amount}
        for rule in rules:
            if rule_matches(rule, row_data):
                entry = create_entry_from_rule(rule, row_data)
                if insert:
                    entry.save()
                entries.append(entry)
                break
        else:
            no_match.append(row_data)
    return entries, no_match


@method_decorator(require_http_methods(["POST"]), name="dispatch")
class AccountUploadCsvView(OwnAccountView):
    template_name = "jagger/upload_csv.html"

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        insert = self.request.POST.get("insert") == "on"
        rules = ImportRule.objects.filter(account=self.object).order_by("id")
        data = self.request.FILES["file"]
        if data.read().startswith(b'"TransferWise ID"'):
            entries, no_match = csv_import_wise(data, rules, insert)
        else:
            entries, no_match = csv_import_ing(data, rules, insert)
        context = super().get_context_data(**kwargs)
        context["table"] = UnsavedEntryTable(entries)
        context["no_match_table"] = NoMatchTable(no_match)
        return self.render_to_response(context=context)
