# django
from django.core.management.base import BaseCommand

# our code
from jagger.models import Account
from jagger.spreadsheet import spreadsheet


class Command(BaseCommand):
    help = "Generates xlsx"

    def handle(self, *args, **options):
        with open("Expenses01.xlsx", "wb") as f:  # TODO"
            f.write(spreadsheet(Account.objects.first()).read())
